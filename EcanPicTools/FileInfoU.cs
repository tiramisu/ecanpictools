﻿using System;

namespace EcanPicTools
{
    public class FileInfoU
    {
        #region 私有变量 Private Fields

        /// <summary>
        /// 图片原始文件夹
        /// </summary>
        private string oldFolderpath;

        /// <summary>
        /// 图片原始全路径
        /// </summary>
        private string oldFullpath;

        /// <summary>
        /// 图片保存文件夹路径
        /// </summary>
        private string newFolderpath;

        /// <summary>
        /// 图片是否重命名[默认False]
        /// </summary>
        private bool isRename;

        #endregion

        #region 公共属性 Public Property

        /// <summary>
        /// 图片原始文件夹
        /// </summary>
        public string OldFolderpath
        {
            get { return oldFolderpath; }
            set { oldFolderpath = value; }
        }

        /// <summary>
        /// 图片原始全路径
        /// </summary>
        public string OldFullpath
        {
            get { return oldFullpath; }
            set { oldFullpath = value; }
        }

        /// <summary>
        /// 图片保存文件夹路径
        /// </summary>
        public string NewFolderpath
        {
            get { return newFolderpath; }
            set { newFolderpath = value; }
        }

        /// <summary>
        /// 图片新全路径
        /// </summary>
        public string NewFullpath
        {
            get
            {
                return NewFolderpath + OldFullpath.Replace(OldFolderpath, "").Replace(OldFileName, "") + NewFileName;
            }
        }

        /// <summary>
        /// 图片后缀
        /// </summary>
        public string FileSuffix
        {
            get
            {
                if (!string.IsNullOrEmpty(OldFileName))
                {
                    int lastpoint = OldFileName.LastIndexOf(@".");
                    if (!lastpoint.Equals(-1))
                    {
                        return OldFileName.Substring(lastpoint + 1);
                    }
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// 图片是否重命名[默认False]
        /// </summary>
        public bool IsRename
        {
            get { return isRename; }
            set { isRename = value; }
        }

        /// <summary>
        /// 图片原文件名[包含后缀]
        /// </summary>
        public string OldFileName
        {
            get
            {
                if (!string.IsNullOrEmpty(OldFullpath))
                {
                    int lastpoint = OldFullpath.LastIndexOf(@"\");
                    if (!lastpoint.Equals(-1))
                    {
                        return OldFullpath.Substring(lastpoint + 1);
                    }
                }
                return string.Empty;
            }
        }
        
        /// <summary>
        /// 图片新文件名[包含后缀]
        /// </summary>
        public string NewFileName
        {
            get
            {
                if(IsRename)
                {
                    return DateTime.Now.ToFileTime() + "." + FileSuffix;
                }
                return OldFileName;
            }
        }

        #endregion
    }
}
