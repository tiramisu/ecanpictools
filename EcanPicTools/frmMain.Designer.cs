﻿namespace EcanPicTools
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnStart = new System.Windows.Forms.Button();
            this.btnShowOpenPath = new System.Windows.Forms.Button();
            this.lboxPicPath = new System.Windows.Forms.ListBox();
            this.btnShowSavePath = new System.Windows.Forms.Button();
            this.txtSavePath = new System.Windows.Forms.TextBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.labInfo = new System.Windows.Forms.Label();
            this.picTop = new System.Windows.Forms.PictureBox();
            this.tbarTransparent = new System.Windows.Forms.TrackBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labTransparent = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pbar = new System.Windows.Forms.ProgressBar();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbtnbili = new System.Windows.Forms.RadioButton();
            this.rbtnkg = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbili = new System.Windows.Forms.TextBox();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbarTransparent)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Location = new System.Drawing.Point(16, 107);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(90, 23);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "开始压缩";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnShowOpenPath
            // 
            this.btnShowOpenPath.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShowOpenPath.Location = new System.Drawing.Point(16, 20);
            this.btnShowOpenPath.Name = "btnShowOpenPath";
            this.btnShowOpenPath.Size = new System.Drawing.Size(90, 23);
            this.btnShowOpenPath.TabIndex = 5;
            this.btnShowOpenPath.Text = "添加文件夹";
            this.btnShowOpenPath.UseVisualStyleBackColor = true;
            this.btnShowOpenPath.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lboxPicPath
            // 
            this.lboxPicPath.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lboxPicPath.FormattingEnabled = true;
            this.lboxPicPath.HorizontalScrollbar = true;
            this.lboxPicPath.Location = new System.Drawing.Point(7, 14);
            this.lboxPicPath.Name = "lboxPicPath";
            this.lboxPicPath.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lboxPicPath.Size = new System.Drawing.Size(268, 264);
            this.lboxPicPath.TabIndex = 3;
            // 
            // btnShowSavePath
            // 
            this.btnShowSavePath.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShowSavePath.Location = new System.Drawing.Point(288, 18);
            this.btnShowSavePath.Name = "btnShowSavePath";
            this.btnShowSavePath.Size = new System.Drawing.Size(106, 23);
            this.btnShowSavePath.TabIndex = 9;
            this.btnShowSavePath.Text = "保存文件夹";
            this.btnShowSavePath.UseVisualStyleBackColor = true;
            this.btnShowSavePath.Click += new System.EventHandler(this.btnShowSavePath_Click);
            // 
            // txtSavePath
            // 
            this.txtSavePath.Location = new System.Drawing.Point(14, 20);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.Size = new System.Drawing.Size(268, 21);
            this.txtSavePath.TabIndex = 4;
            // 
            // btnSelect
            // 
            this.btnSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelect.Location = new System.Drawing.Point(16, 49);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(90, 23);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "添加单个文件";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // labInfo
            // 
            this.labInfo.AutoSize = true;
            this.labInfo.Location = new System.Drawing.Point(12, 430);
            this.labInfo.Name = "labInfo";
            this.labInfo.Size = new System.Drawing.Size(125, 12);
            this.labInfo.TabIndex = 7;
            this.labInfo.Text = "已经压缩图片张数:0张";
            // 
            // picTop
            // 
            this.picTop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picTop.Image = ((System.Drawing.Image)(resources.GetObject("picTop.Image")));
            this.picTop.Location = new System.Drawing.Point(11, 15);
            this.picTop.Name = "picTop";
            this.picTop.Size = new System.Drawing.Size(32, 32);
            this.picTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picTop.TabIndex = 8;
            this.picTop.TabStop = false;
            this.picTop.Click += new System.EventHandler(this.picTop_Click);
            // 
            // tbarTransparent
            // 
            this.tbarTransparent.AutoSize = false;
            this.tbarTransparent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tbarTransparent.Location = new System.Drawing.Point(49, 18);
            this.tbarTransparent.Maximum = 100;
            this.tbarTransparent.Name = "tbarTransparent";
            this.tbarTransparent.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbarTransparent.Size = new System.Drawing.Size(233, 30);
            this.tbarTransparent.TabIndex = 9;
            this.tbarTransparent.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbarTransparent.Scroll += new System.EventHandler(this.tbarTransparent_Scroll);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labTransparent);
            this.groupBox1.Controls.Add(this.picTop);
            this.groupBox1.Controls.Add(this.tbarTransparent);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(413, 50);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基本设置";
            // 
            // labTransparent
            // 
            this.labTransparent.AutoSize = true;
            this.labTransparent.Location = new System.Drawing.Point(300, 21);
            this.labTransparent.Name = "labTransparent";
            this.labTransparent.Size = new System.Drawing.Size(47, 12);
            this.labTransparent.TabIndex = 10;
            this.labTransparent.Text = "透明值:";
            // 
            // btnDelete
            // 
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Location = new System.Drawing.Point(16, 78);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "移除选中文件";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lboxPicPath);
            this.groupBox2.Location = new System.Drawing.Point(12, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(282, 291);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "图片路径";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnSelect);
            this.groupBox3.Controls.Add(this.btnShowOpenPath);
            this.groupBox3.Controls.Add(this.btnStart);
            this.groupBox3.Location = new System.Drawing.Point(298, 218);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(125, 141);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "操作";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnShowSavePath);
            this.groupBox4.Controls.Add(this.txtSavePath);
            this.groupBox4.Location = new System.Drawing.Point(12, 365);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(413, 48);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "选择保存文件夹";
            // 
            // pbar
            // 
            this.pbar.Location = new System.Drawing.Point(190, 426);
            this.pbar.Name = "pbar";
            this.pbar.Size = new System.Drawing.Size(233, 21);
            this.pbar.TabIndex = 17;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtHeight);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.txtWidth);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.txtbili);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.rbtnkg);
            this.groupBox5.Controls.Add(this.rbtnbili);
            this.groupBox5.Location = new System.Drawing.Point(300, 68);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(123, 144);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "压缩条件";
            // 
            // rbtnbili
            // 
            this.rbtnbili.AutoSize = true;
            this.rbtnbili.Checked = true;
            this.rbtnbili.Location = new System.Drawing.Point(14, 18);
            this.rbtnbili.Name = "rbtnbili";
            this.rbtnbili.Size = new System.Drawing.Size(83, 16);
            this.rbtnbili.TabIndex = 0;
            this.rbtnbili.TabStop = true;
            this.rbtnbili.Text = "按比例压缩";
            this.rbtnbili.UseVisualStyleBackColor = true;
            this.rbtnbili.CheckedChanged += new System.EventHandler(this.rbtnbili_CheckedChanged);
            // 
            // rbtnkg
            // 
            this.rbtnkg.AutoSize = true;
            this.rbtnkg.Location = new System.Drawing.Point(14, 67);
            this.rbtnkg.Name = "rbtnkg";
            this.rbtnkg.Size = new System.Drawing.Size(83, 16);
            this.rbtnkg.TabIndex = 2;
            this.rbtnkg.Text = "按宽高压缩";
            this.rbtnkg.UseVisualStyleBackColor = true;
            this.rbtnkg.CheckedChanged += new System.EventHandler(this.rbtnkg_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "比例值:";
            // 
            // txtbili
            // 
            this.txtbili.Location = new System.Drawing.Point(65, 40);
            this.txtbili.Name = "txtbili";
            this.txtbili.Size = new System.Drawing.Size(32, 21);
            this.txtbili.TabIndex = 1;
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(65, 89);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(41, 21);
            this.txtWidth.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "宽度值:";
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(65, 116);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(41, 21);
            this.txtHeight.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "高度值:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(100, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "%";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 458);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.pbar);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "图片压缩工具（作者：刘典武）";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbarTransparent)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnShowOpenPath;
        private System.Windows.Forms.ListBox lboxPicPath;
        private System.Windows.Forms.Button btnShowSavePath;
        private System.Windows.Forms.TextBox txtSavePath;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Label labInfo;
        private System.Windows.Forms.PictureBox picTop;
        private System.Windows.Forms.TrackBar tbarTransparent;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labTransparent;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ProgressBar pbar;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbtnbili;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbili;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnkg;
        private System.Windows.Forms.Label label4;
    }
}

