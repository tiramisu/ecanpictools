﻿using System;
using System.Windows.Forms;
using System.IO;
//using SKINPPDOTNETLib;

namespace EcanPicTools
{
    public partial class frmMain : Form
    {
        //SKINPPDOTNETLib.SkinPPDotNetClass myskin = new SkinPPDotNetClass();
        public frmMain()
        {
            InitializeComponent();
            this.txtbili.KeyPress += new KeyPressEventHandler(txt_KeyPress);
            this.txtWidth.KeyPress += new KeyPressEventHandler(txt_KeyPress);
            this.txtHeight.KeyPress += new KeyPressEventHandler(txt_KeyPress);
            CheckForIllegalCrossThreadCalls = false;
            //myskin.LoadSkin(Application.StartupPath + @"\spring.ssk", true);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            init();
        }

        private void init()
        {
            this.Text = "图片压缩工具（作者：刘典武）---普通模式";
            labTransparent.Text = "透明值:100%";
            txtWidth.Enabled = false;
            txtHeight.Enabled = false;
            rbtnbili.Checked = true;
            txtbili.Focus();
        }

        private void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8))
            {
                e.Handled = true;
            }
            base.OnKeyPress(e);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (lboxPicPath.Items.Count <= 0)
            {
                MessageBox.Show("你还没有选择要压缩的图片！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtSavePath.Text == "")
            {
                MessageBox.Show("你还没有选择要保存的文件夹路径！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            pbar.Maximum = lboxPicPath.Items.Count;
            pbar.Value = 0;

            if (rbtnbili.Checked && txtbili.Text == "")
            {
                MessageBox.Show("请填好比例值！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtbili.Focus();
                return;
            }
            if (rbtnkg.Checked && (txtHeight.Text == "" || txtWidth.Text == ""))
            {
                MessageBox.Show("请填好宽高值！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtWidth.Focus();
                return;
            }

            for (int i = 0; i < lboxPicPath.Items.Count; i++)
            {
                pbar.Value = i + 1;
                string sourceFile = lboxPicPath.Items[i].ToString();
                string suffix = sourceFile.Substring(sourceFile.LastIndexOf("."));
                Thumbnail.GetThumbnail(sourceFile, txtSavePath.Text + "\\" + DateTime.Now.ToFileTime() + suffix, Convert.ToDouble(txtbili.Text),50);
                //this.yasuo(lboxPicPath.Items[i].ToString(), txtSavePath.Text + "\\" + DateTime.Now.ToLongTimeString());
                labInfo.Text = "已经压缩图片张数:" + Convert.ToString(i + 1);
            }
            MessageBox.Show("恭喜，压缩图片成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                this.getFile(fbd.SelectedPath);
            }
        }

        private void getFile(string path)
        {
            DirectoryInfo pic = new DirectoryInfo(path);
            foreach (FileInfo file in pic.GetFiles("*.*"))
            {
                lboxPicPath.Items.Add(file.FullName);
            }
        }

        private void btnShowSavePath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "请选择保存输出图像路径";
            fbd.ShowNewFolderButton = true;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                if (fbd.SelectedPath != "")
                {
                    txtSavePath.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "请选择要压缩的图片";
            open.Filter = "图片文件(*.jpg,*.bmp,*.png,*.gif)|*.jpg;*.bmp;*.png;*.gif";
            open.Multiselect = true;
            if (open.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in open.FileNames)
                {
                    lboxPicPath.Items.Add(file);
                }
            }
        }

        private void picTop_Click(object sender, EventArgs e)
        {
            if (this.TopMost)
            {
                this.TopMost = false;
                this.Text = "图片压缩工具（作者：刘典武）---普通模式";
            }
            else
            {
                this.TopMost = true;
                this.Text = "图片压缩工具（作者：刘典武）---置顶模式";
            }
        }

        private void tbarTransparent_Scroll(object sender, EventArgs e)
        {
            labTransparent.Text = "透明值:" + Convert.ToString(100 - tbarTransparent.Value) + "%";

            this.Opacity = 1 - (float)(tbarTransparent.Value) / 100;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lboxPicPath.SelectedItems.Count > 0)
            {
                for (int i = lboxPicPath.SelectedItems.Count - 1; i >= 0; i--)
                {
                    lboxPicPath.Items.Remove(lboxPicPath.SelectedItems[i]);
                }
            }
            else
            {
                MessageBox.Show("请选择要移除的文件", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void rbtnbili_CheckedChanged(object sender, EventArgs e)
        {
            txtbili.Enabled = rbtnbili.Checked;
            if (rbtnbili.Checked)
            {
                txtbili.Focus();
            }
        }

        private void rbtnkg_CheckedChanged(object sender, EventArgs e)
        {
            txtWidth.Enabled = rbtnkg.Checked;
            txtHeight.Enabled = rbtnkg.Checked;
            if (rbtnkg.Checked)
            {
                txtWidth.Focus();
            }
        }

    }
}