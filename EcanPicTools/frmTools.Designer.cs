﻿namespace EcanPicTools
{
    partial class frmTools
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTools));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.msAddFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.msAddImgFile = new System.Windows.Forms.ToolStripMenuItem();
            this.msClearlbox = new System.Windows.Forms.ToolStripMenuItem();
            this.msRemoveSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.msStartEcanpic = new System.Windows.Forms.ToolStripMenuItem();
            this.msHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioReName = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPercentPixel = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnStartEcanPic = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPercentCom = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnShowSavePath = new System.Windows.Forms.Button();
            this.txtSavePath = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsComCount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsAllCount = new System.Windows.Forms.ToolStripLabel();
            this.tsProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.tsPrompt = new System.Windows.Forms.ToolStripLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lboxPicPath = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolItemDeleteSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.msAddFolder,
            this.msAddImgFile,
            this.msClearlbox,
            this.msRemoveSelected,
            this.msStartEcanpic,
            this.msHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(557, 25);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // msAddFolder
            // 
            this.msAddFolder.Name = "msAddFolder";
            this.msAddFolder.Size = new System.Drawing.Size(80, 21);
            this.msAddFolder.Text = "添加文件夹";
            this.msAddFolder.Click += new System.EventHandler(this.msAddFolder_Click);
            // 
            // msAddImgFile
            // 
            this.msAddImgFile.Name = "msAddImgFile";
            this.msAddImgFile.Size = new System.Drawing.Size(92, 21);
            this.msAddImgFile.Text = "添加单个图片";
            this.msAddImgFile.Click += new System.EventHandler(this.msAddImgFile_Click);
            // 
            // msClearlbox
            // 
            this.msClearlbox.Name = "msClearlbox";
            this.msClearlbox.Size = new System.Drawing.Size(68, 21);
            this.msClearlbox.Text = "清空列表";
            this.msClearlbox.Click += new System.EventHandler(this.msClearlbox_Click);
            // 
            // msRemoveSelected
            // 
            this.msRemoveSelected.Name = "msRemoveSelected";
            this.msRemoveSelected.Size = new System.Drawing.Size(68, 21);
            this.msRemoveSelected.Text = "删除选中";
            this.msRemoveSelected.Click += new System.EventHandler(this.msRemoveSelected_Click);
            // 
            // msStartEcanpic
            // 
            this.msStartEcanpic.Name = "msStartEcanpic";
            this.msStartEcanpic.Size = new System.Drawing.Size(68, 21);
            this.msStartEcanpic.Text = "开始压缩";
            this.msStartEcanpic.Click += new System.EventHandler(this.msStartEcanpic_Click);
            // 
            // msHelp
            // 
            this.msHelp.Name = "msHelp";
            this.msHelp.Size = new System.Drawing.Size(68, 21);
            this.msHelp.Text = "使用帮助";
            this.msHelp.Click += new System.EventHandler(this.msHelp_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioReName);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtPercentPixel);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.btnStartEcanPic);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txtPercentCom);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnShowSavePath);
            this.groupBox4.Controls.Add(this.txtSavePath);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox4.Size = new System.Drawing.Size(557, 79);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "压缩配置";
            // 
            // radioReName
            // 
            this.radioReName.AutoSize = true;
            this.radioReName.Location = new System.Drawing.Point(290, 52);
            this.radioReName.Name = "radioReName";
            this.radioReName.Size = new System.Drawing.Size(84, 16);
            this.radioReName.TabIndex = 19;
            this.radioReName.Text = "图片重命名";
            this.radioReName.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(250, 53);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 18;
            this.label3.Text = "%";
            // 
            // txtPercentPixel
            // 
            this.txtPercentPixel.Location = new System.Drawing.Point(215, 50);
            this.txtPercentPixel.Name = "txtPercentPixel";
            this.txtPercentPixel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPercentPixel.Size = new System.Drawing.Size(32, 21);
            this.txtPercentPixel.TabIndex = 16;
            this.txtPercentPixel.Text = "50";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(144, 53);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "像素比值：";
            // 
            // btnStartEcanPic
            // 
            this.btnStartEcanPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStartEcanPic.Location = new System.Drawing.Point(447, 46);
            this.btnStartEcanPic.Name = "btnStartEcanPic";
            this.btnStartEcanPic.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnStartEcanPic.Size = new System.Drawing.Size(106, 23);
            this.btnStartEcanPic.TabIndex = 15;
            this.btnStartEcanPic.Text = "开始压缩";
            this.btnStartEcanPic.UseVisualStyleBackColor = true;
            this.btnStartEcanPic.Click += new System.EventHandler(this.msStartEcanpic_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(118, 53);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 13;
            this.label4.Text = "%";
            // 
            // txtPercentCom
            // 
            this.txtPercentCom.Location = new System.Drawing.Point(83, 50);
            this.txtPercentCom.Name = "txtPercentCom";
            this.txtPercentCom.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPercentCom.Size = new System.Drawing.Size(32, 21);
            this.txtPercentCom.TabIndex = 11;
            this.txtPercentCom.Text = "50";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "压缩比值：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "保存地址：";
            // 
            // btnShowSavePath
            // 
            this.btnShowSavePath.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShowSavePath.Location = new System.Drawing.Point(447, 18);
            this.btnShowSavePath.Name = "btnShowSavePath";
            this.btnShowSavePath.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnShowSavePath.Size = new System.Drawing.Size(106, 23);
            this.btnShowSavePath.TabIndex = 9;
            this.btnShowSavePath.Text = "选择文件夹";
            this.btnShowSavePath.UseVisualStyleBackColor = true;
            this.btnShowSavePath.Click += new System.EventHandler(this.btnShowSavePath_Click);
            // 
            // txtSavePath
            // 
            this.txtSavePath.Location = new System.Drawing.Point(83, 18);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSavePath.Size = new System.Drawing.Size(360, 21);
            this.txtSavePath.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(557, 83);
            this.panel2.TabIndex = 10;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tsComCount,
            this.toolStripLabel2,
            this.tsAllCount,
            this.tsProgressBar,
            this.tsPrompt});
            this.toolStrip1.Location = new System.Drawing.Point(0, 346);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(557, 25);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(104, 22);
            this.toolStripLabel1.Text = "已压缩图片张数：";
            // 
            // tsComCount
            // 
            this.tsComCount.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.tsComCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.tsComCount.Name = "tsComCount";
            this.tsComCount.Size = new System.Drawing.Size(15, 22);
            this.tsComCount.Text = "0";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(80, 22);
            this.toolStripLabel2.Text = "总图片张数：";
            // 
            // tsAllCount
            // 
            this.tsAllCount.Name = "tsAllCount";
            this.tsAllCount.Size = new System.Drawing.Size(15, 22);
            this.tsAllCount.Text = "0";
            // 
            // tsProgressBar
            // 
            this.tsProgressBar.Name = "tsProgressBar";
            this.tsProgressBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsProgressBar.Size = new System.Drawing.Size(200, 22);
            // 
            // tsPrompt
            // 
            this.tsPrompt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.tsPrompt.ForeColor = System.Drawing.Color.Green;
            this.tsPrompt.Name = "tsPrompt";
            this.tsPrompt.Size = new System.Drawing.Size(56, 22);
            this.tsPrompt.Text = "压缩提示";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lboxPicPath);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 108);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(557, 238);
            this.panel1.TabIndex = 12;
            // 
            // lboxPicPath
            // 
            this.lboxPicPath.AllowDrop = true;
            this.lboxPicPath.ContextMenuStrip = this.contextMenuStrip1;
            this.lboxPicPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lboxPicPath.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lboxPicPath.FormattingEnabled = true;
            this.lboxPicPath.HorizontalScrollbar = true;
            this.lboxPicPath.Location = new System.Drawing.Point(0, 0);
            this.lboxPicPath.Name = "lboxPicPath";
            this.lboxPicPath.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lboxPicPath.Size = new System.Drawing.Size(557, 238);
            this.lboxPicPath.TabIndex = 5;
            this.lboxPicPath.DragDrop += new System.Windows.Forms.DragEventHandler(this.lboxPicPath_DragDrop);
            this.lboxPicPath.DragEnter += new System.Windows.Forms.DragEventHandler(this.lboxPicPath_DragEnter);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolItemDeleteSelected});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            // 
            // ToolItemDeleteSelected
            // 
            this.ToolItemDeleteSelected.Name = "ToolItemDeleteSelected";
            this.ToolItemDeleteSelected.Size = new System.Drawing.Size(124, 22);
            this.ToolItemDeleteSelected.Text = "删除选中";
            this.ToolItemDeleteSelected.Click += new System.EventHandler(this.msRemoveSelected_Click);
            // 
            // frmTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 371);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmTools";
            this.Text = "图片压缩工具（作者：传德）";
            this.Load += new System.EventHandler(this.frmTools_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem msClearlbox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnShowSavePath;
        private System.Windows.Forms.TextBox txtSavePath;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem msStartEcanpic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPercentCom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel tsComCount;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox lboxPicPath;
        private System.Windows.Forms.ToolStripMenuItem msAddFolder;
        private System.Windows.Forms.ToolStripMenuItem msAddImgFile;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel tsAllCount;
        private System.Windows.Forms.ToolStripProgressBar tsProgressBar;
        private System.Windows.Forms.Button btnStartEcanPic;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPercentPixel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripLabel tsPrompt;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem msRemoveSelected;
        private System.Windows.Forms.ToolStripMenuItem msHelp;
        private System.Windows.Forms.CheckBox radioReName;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolItemDeleteSelected;

    }
}